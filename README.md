
## Project setup
```
npm install
```

### Deploy locally
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```